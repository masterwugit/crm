package com.huike.review.service;

import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;

import java.util.List;

/**
 * mybatis复习接口层
 */
public interface ReviewService {
    /**
     * 添加用户
     * @param review
     */
    void saveData(Review review);

    /**
     * 删除用户
     * @param id
     */
    void deleteData(Long id);
    /**
     * 修改用户
     */
    void update(Review review);
    /**
     * 通过id查询
     */
    Review getById(Long id);

    /**
     * 分页查询
     * @param pageNum
     * @param PageSize
     * @return
     */
    TableDataInfo getDataByPage(Integer pageNum,Integer PageSize);

}
