package com.huike.review.mapper;

import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * Mybatis复习的Mapper层
 */
public interface MybatisReviewMapper {

    /**======================================================新增======================================================**/
    void saveData(Review review);
    /**======================================================删除======================================================**/
    void deleteData(Long id);
    /**======================================================修改======================================================**/
    void update(Review review);
    /**======================================================简单查询===================================================**/
    //通过id查询单个数据
    Review getById(Long id);
    //分页查询
    List<Review> getDataByPage();

}
