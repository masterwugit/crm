package com.huike.web.controller.review;


import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;
import com.huike.review.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 该Controller主要是为了复习三层架构以及Mybatis使用的，该部分接口已经放开权限，可以直接访问
 * 同学们在此处编写接口通过浏览器访问看是否能完成最简单的增删改查
 */
@RestController
@RequestMapping("/review")
public class MybatisReviewController extends BaseController {

    @Autowired
    private ReviewService reviewService;

    /**新增数据*/
    @GetMapping("saveData")
    public AjaxResult saveData(@RequestBody Review review){
        reviewService.saveData(review);
        return AjaxResult.success("操作成功");
    }
    /**删除数据*/
    @DeleteMapping("remove/{id}")
    public AjaxResult deleteById(@PathVariable Long id){
        reviewService.deleteData(id);
        return AjaxResult.success("删除成功");
    }
    /**修改数据*/
    @PostMapping("update")
    public AjaxResult update(@RequestBody Review review){
        reviewService.update(review);
        return AjaxResult.success("修改成功");
    }
    /**通过id查询数据*/
    @GetMapping("getById")
    public AjaxResult getById(Long id){
        Review review = reviewService.getById(id);
        return AjaxResult.success("查询成功",review);
    }
    /**分页查询*/
    @GetMapping("getDataByPage")
    public TableDataInfo getDataByPage(Integer pageNum,Integer pageSize){
        TableDataInfo page = reviewService.getDataByPage(pageNum, pageSize);
        page.setCode(200);
        page.setMsg("查询成功");
        return page;
    }
}