package com.huike.report.mapper;

import java.util.Map;

import com.huike.report.domain.vo.IndexTodayInfoVO;
import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.vo.IndexStatisticsVo;

/**
 * 首页统计分析的Mapper
 * @author Administrator
 *
 */
public interface ReportMapper {
	/**=========================================基本数据========================================*/
	/**
	 * 获取线索数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getCluesNum(@Param("startTime") String beginCreateTime,
						@Param("endTime") String endCreateTime,
						@Param("username") String username);

	/**
	 * 获取商机数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getBusinessNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getContractNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同金额
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Double getSalesAmount(@Param("startTime") String beginCreateTime,
						  @Param("endTime") String endCreateTime,
						  @Param("username") String username);

	/**=========================================今日简报========================================*/
	/**
	 * 获取线索数
	 * @param userId
	 * @param toDay
	 * @return
	 */
	Integer getToDayCluesNum(@Param("userId") Long userId,@Param("toDay") String toDay);

	/**
	 * 获取商机数
	 * @param userId
	 * @param toDay
	 * @return
	 */
	Integer getToDayBusinessNum(@Param("userId") Long userId,@Param("toDay") String toDay);

	/**
	 * 今日合同
	 * @param username
	 * @param toDay
	 * @return
	 */
	Integer getTodayContractNum(@Param("username") String username,@Param("toDay") String toDay);

	/**
	 * 今日金额
	 * @param username
	 * @param toDay
	 * @return
	 */
	Double getTodaySalesAmount(@Param("username") String username,@Param("toDay") String toDay);



	/**=========================================待办========================================*/

}
